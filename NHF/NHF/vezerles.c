#define _CRT_SECURE_NO_WARNINGS
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "econio.h"
#include "fajlkezeles.h"
#include "megjelenites.h"
#include "vezerles.h"

/* egy üres malom tábla */
const char palya[7][7] = {
	"0  0  0",
	" 0 0 0 ",
	"  000  ",
	"000.000",
	"  000  ",
	" 0 0 0 ",
	"0  0  0"
};

/* ellenőrzi, hogy sikerult e memóriat foglalni, az adott címen, ha nem akkor kilép */
void malom_memoria_foglalva(void *p) {
	if (p == NULL) {
		econio_textcolor(LIGHTRED);
		printf("NEM SIKERULT A MEMORIAFOGLALAS!");
		econio_sleep(3);
		exit(0);
	}
}

/* az induló játék struktúrájának elkészítése */
Malom *malom_uj() {
	Malom* m = (Malom*)malloc(sizeof(Malom));
	malom_memoria_foglalva(m);
	for (int y = 0; y < 7; y++)
		for (int x = 0; x < 7; x++)
			m->palya[y][x] = palya[y][x];
	m->palyaArany = 1;
	m->jatekMod = 2;
	m->jatekFazis = f1;
	m->kovJ = j1;
	m->kovL = l1;
	m->poz = (Poz) { -1, -1 };
	m->j1 = (Jatekos) { NULL, LIGHTBLUE, 9 };
	m->j2 = (Jatekos) { NULL, LIGHTRED, 9 };
	m->lepesek = NULL;
	m->debug = 0;
	return m;
}

/* a lépések listájába szúrja be a sorra következő lépést */
void malom_lepesek_beszur(Malom *m, LepesTipus lepes, Poz A, Poz B) {
	Lepes *uj = (Lepes*)malloc(sizeof(Lepes));
	malom_memoria_foglalva(uj);
	uj->lepes = lepes;
	uj->A = A;
	uj->B = B;
	if (m->lepesek == NULL) {
		m->lepesek = (Lepesek*)malloc(sizeof(Lepesek));
		malom_memoria_foglalva(m->lepesek);
		m->lepesek->elso = (Lepes*)malloc(sizeof(Lepes));
		malom_memoria_foglalva(m->lepesek->elso);
		m->lepesek->utolso = (Lepes*)malloc(sizeof(Lepes));
		malom_memoria_foglalva(m->lepesek->utolso);
		uj->elozo = m->lepesek->elso;
		uj->kov = m->lepesek->utolso;
		m->lepesek->elso->kov = uj;
		m->lepesek->utolso->elozo = uj;
	}
	else {
		uj->elozo = m->lepesek->utolso->elozo;
		uj->kov = m->lepesek->utolso;
		m->lepesek->utolso->elozo->kov = uj;
		m->lepesek->utolso->elozo = uj;
	}
}

/* a lépések listájának felszabadítása */
static void malom_lepesek_felszabadit(Malom *m) {
	Lepes *mozgo = m->lepesek->elso;
	while (mozgo != m->lepesek->utolso) {
		Lepes *kov = mozgo->kov;
		free(mozgo);
		mozgo = kov;
	}
	free(m->lepesek->utolso);
	free(m->lepesek);
}

/* 
 * A VEZÉRLÉST SEGÍTŐ FÜGGVÉNYEK
 */

/* ellenőrzi, hogy a következő lépésként adott pozíción üres-e */
static bool malom_ures(Malom *m) {
	return m->palya[m->poz.y][m->poz.x] == '0';
}

/* ellenőrzi, hogy a következő lépésként adott pozíción van-e bábú */
static bool malom_foglalt(Malom *m) {
	return m->palya[m->poz.y][m->poz.x] == '1' || m->palya[m->poz.y][m->poz.x] == '2';
}

/* ellenőrzi, hogy a következő lépésként adott pozíción tárolt adat egyezik-e a játékos karakterével */
static bool malom_azonos(Malom *m) {
	return m->palya[m->poz.y][m->poz.x] == (m->kovJ == j1 ? '1' : '2');
}

/* ellenőrzi, hogy a legutóbb vizsgált pozíció része egy malomnak */
static bool malom_resze(Malom *m) {
	int db = 0; // azonos babuk szama
	/* vízszintes menetelés */
	for (int x = 0; x < 7; x++)
		if (m->palya[m->poz.y][x] == m->palya[m->poz.y][m->poz.x])
			if (m->poz.y == 3) {
				if (m->poz.x < 3 && x < 3)
					db++;
				else if (m->poz.x > 3 && x > 3)
					db++;
			}
			else
				db++;
	if (db == 3)
		return true;
	db = 0;
	/* függőleges menetelés */
	for (int y = 0; y < 7; y++)
		if (m->palya[y][m->poz.x] == m->palya[m->poz.y][m->poz.x])
			if (m->poz.x == 3) {
				if (m->poz.y < 3 && y < 3)
					db++;
				else if (m->poz.y > 3 && y > 3)
					db++;
			}
			else
				db++;
	if (db == 3)
		return true;
	return false;
}

/* azt vizsgálja, hogy az ellenfél minden babuja malomba áll-e */
static bool malom_resze_osszes(Malom *m) {
	Poz temp = { m->poz.x, m->poz.y };
	char babu = (m->kovJ == j1 ? '2' : '1');
	for (int y = 0; y < 7; y++)
		for (int x = 0; x < 7; x++) {
			m->poz = (Poz) { x, y };
			if (babu == m->palya[m->poz.y][m->poz.x] && !malom_resze(m)) {
				m->poz = (Poz) { temp.x, temp.y };
				return false;
			}
		}
	m->poz = (Poz) { temp.x, temp.y };
	return true;
}

/* a pályára elhelyez egy bábút, ha a beolvasott pozició megfelel a feltételeknek, azaz üres (ez a lépés csak az első fázisban futhat le) */
static void malom_letesz(Malom *m) {
	while (!malom_poz_olvas(m) || !malom_ures(m));
	m->palya[m->poz.y][m->poz.x] = (m->kovJ == j1 ? '1' : '2');
	malom_lepesek_beszur(m, l1, m->poz, m->poz);
}

/*
 * a pályáról elvesz egy bábút, ha a beolvasott pozició megfelel a feltételeknek
 * két eset van, ha az ellenfél összes bábúja malomban áll, akkor csak, arra kell figyelni, hogy ne sajátot vegyünk le
 * a másik esetben ezen felül vizsgáljuk, azt hogy foglalt-e, azaz van-e ott babú, illetve malomban áll-e(akkor nem vehetjük le) 
 */
static void malom_elvesz(Malom *m) {
	if (malom_resze_osszes(m))
		while (!malom_poz_olvas(m) || malom_azonos(m));
	else
		while (!malom_poz_olvas(m) || !malom_foglalt(m) || malom_azonos(m) || malom_resze(m));
	m->palya[m->poz.y][m->poz.x] = '0';
	malom_lepesek_beszur(m, l2, m->poz, m->poz);
}

/* igazat ad, ha egy adott pozicíóra 1 lepesel el lehet jutni */
static bool malom_poz_tav(Malom *m, Poz babu) {
	if (babu.x != m->poz.x && babu.y != m->poz.y)
		return false;
	if (babu.x == m->poz.x && babu.y == m->poz.y)
		return false;
	if (babu.x == m->poz.x)
		if (babu.y < m->poz.y)
			for (int y = babu.y+1; y < 7; y++) {
				if (y == m->poz.y)
					return true;
				else if (m->palya[y][babu.x] != ' ')
					return false;
			}
		else
			for (int y = babu.y-1; y >= 0; y--) {
				if (y == m->poz.y)
					return true;
				else if (m->palya[y][babu.x] != ' ')
					return false;
			}
	if (babu.y == m->poz.y)
		if (babu.x < m->poz.x)
			for (int x = babu.x+1; x < 7; x++) {
				if (x == m->poz.x)
					return true;
				else if (m->palya[babu.y][x] != ' ')
					return false;
			}
		else
			for (int x = babu.x-1; x >= 0; x--) {
				if (x == m->poz.x)
					return true;
				else if (m->palya[babu.y][x] != ' ')
					return false;
			}
	return false;
}

/* igazat ad, ha a vizsgált pozicióból el lehet lépni */
static bool malom_poz_tudlepni(Malom *m) {
	if (m->poz.y != 7-1)
		for (int y = m->poz.y+1; y < 7; y++) {
			if (m->palya[y][m->poz.x] == '0')
				return true;
			else if (m->palya[y][m->poz.x] != ' ')
				break;
		}
	if (m->poz.y != 0)
		for (int y = m->poz.y-1; y >= 0; y--) {
			if (m->palya[y][m->poz.x] == '0')
				return true;
			else if (m->palya[y][m->poz.x] != ' ')
				break;
		}
	if (m->poz.x != 7-1)
		for (int x = m->poz.x+1; x < 7; x++) {
			if (m->palya[m->poz.y][x] == '0')
				return true;
			else if (m->palya[m->poz.y][x] != ' ')
				break;
		}
	if (m->poz.x != 0)
		for (int x = m->poz.x-1; x >= 0; x--) {
			if (m->palya[m->poz.y][x] == '0')
				return true;
			else if (m->palya[m->poz.y][x] != ' ')
				break;
		}
	return false;
}

/* azt vizsglja, hogy a játékos valamennyi bábújával képtelen lépni */
static bool malom_jatekos_nemtudlepni(Malom *m){
	for (int y = 0; y < 7; y++)
		for (int x = 0; x < 7; x++)
			if (m->palya[y][x] == (m->kovJ == j1 ? '1' : '2')) {
				m->poz = (Poz) { x, y };
				if (malom_poz_tudlepni(m))
					return false;
			}
	return true;
}

/* megmondja az aktuálisan következő játékos babúinak számát a pályán */
static int malom_jatekos_babukszama(Malom *m) {
	int db = 0;
	for (int y = 0; y < 7; y++)
		for (int x = 0; x < 7; x++)
			if (m->palya[y][x] == (m->kovJ == j1 ? '1' : '2'))
				db++;
	return db;
}

/* átlépés egy másik pozicióra (csak mellette levo ures mezore)  */
static void malom_athelyez(Malom *m) {
	while (!malom_poz_olvas(m) || !malom_azonos(m) || !malom_poz_tudlepni(m));
	Poz babu = { m->poz.x, m->poz.y };
	m->kovL = l4;
	while (!malom_poz_olvas(m) || !malom_ures(m) || !malom_poz_tav(m,babu));
	m->palya[babu.y][babu.x] = '0';
	m->palya[m->poz.y][m->poz.x] = (m->kovJ == j1 ? '1' : '2');
	malom_lepesek_beszur(m, l3, babu, m->poz);
}

/* ugrás egy másik pozicióra (bárhova, ahol nem áll babá) */
static void malom_ugras(Malom *m) {
	while (!malom_poz_olvas(m) || !malom_azonos(m));
	Poz babu = { m->poz.x, m->poz.y };
	m->kovL = l6;
	while (!malom_poz_olvas(m) || !malom_ures(m));
	m->palya[babu.y][babu.x] = '0';
	m->palya[m->poz.y][m->poz.x] = (m->kovJ == j1 ? '1' : '2');
	malom_lepesek_beszur(m, l5, babu, m->poz);
}

/* megváltoztatja a struktúrában, a következő játékost */
static void malom_kovJ(Malom *m) {
		if (m->kovJ == j1)	m->kovJ = j2;
		else				m->kovJ = j1;
}

/* kivon egyet a játékos számára rendelkezésre álló bábuk számából */
static void malom_babu_kivon(Malom *m) {
	if (m->kovJ == j1)	m->j1.db--;
	else				m->j2.db--;
}

/* ellenőrzi, hogy át kell-e térni a következő fázisba */
static bool malom_fazis(Malom *m) {
	if (m->jatekFazis == f1 && m->j1.db + m->j2.db == 0) {
		m->jatekFazis = f2;
		m->kovL = l3;
		return true;
	}
	return false;
}

/* a lefoglalt memóriaterület felszabadítása */
void malom_free(Malom *m) {
	/* a dinamikusan foglalt nevek felszabadítása */
	free(m->j1.nev); 
	free(m->j2.nev);
	/* a lépések lista felszabadítása */
	malom_lepesek_felszabadit(m);
	/* a játék struktúra felszabadítása */
	free(m);
}

/* a játék vezérlését végző függvény */
void malom_jatek(Malom *m) {
	bool malom_vege = false;
	while (!malom_vege) {
		if (!malom_fajl_mentes(m)) {
			econio_textcolor(LIGHTRED);
			printf("NEM SIKERULT A MENTES!\n");
			econio_textcolor(LIGHTCYAN);
		}
		malom_kirajzol(m);
		switch (m->jatekFazis) {
			case f1:
				if (m->kovL == l1) {
					if (malom_fazis(m))
						break;
					malom_letesz(m);
					malom_babu_kivon(m);
					if (malom_resze(m))
						m->kovL = l2;
					else
						malom_kovJ(m);
				}
				else if (m->kovL == l2) {
					malom_elvesz(m);
					malom_kovJ(m);
					if (malom_fazis(m))
						break;
					m->kovL = l1;
				}
				break;
			case f2:
				if (malom_jatekos_nemtudlepni(m) || malom_jatekos_babukszama(m) == 2) {
					malom_vege = true;
					break;
				}
				if (m->kovL != l2) {
					if (malom_jatekos_babukszama(m) == 3)
						m->kovL = l5;
					else
						m->kovL = l3;
				}
				if (m->kovL == l3) {
					malom_athelyez(m);
					if (malom_resze(m))
						m->kovL = l2;
					else
						malom_kovJ(m);
				}
				else if (m->kovL == l5) {
					malom_ugras(m);
					if (malom_resze(m))
						m->kovL = l2;
					else
						malom_kovJ(m);
				}
				else if (m->kovL == l2) {
					malom_elvesz(m);
					malom_kovJ(m);
					m->kovL = l3;
				}
				break;
		}
	}
}