#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#include "fajlkezeles.h"
#include "megjelenites.h"
#include "vezerles.h"

/* ki�rja a l�p�sek list�j�t a megadott kimenetre (f�jl vagy stdout) */
void malom_lepesek_kiir(Malom *m, FILE *fp) {
	if (m->lepesek == NULL) {
		fprintf(fp, "\n");
		return;
	}
	Lepes *mozgo = m->lepesek->elso->kov;
	while (mozgo != m->lepesek->utolso) {
		if (mozgo->lepes == l1)
			fprintf(fp, "%c%d;", mozgo->A.x + 'a', mozgo->A.y);
		else if (mozgo->lepes == l2)
			fprintf(fp, "x%c%d;", mozgo->A.x + 'a', mozgo->A.y);
		else if (mozgo->lepes == l3 || mozgo->lepes == l5)
			fprintf(fp, "%c%d-%c%d;", mozgo->A.x + 'a', mozgo->A.y, mozgo->B.x + 'a', mozgo->B.y);
		mozgo = mozgo->kov;
	}
	fprintf(fp, "\n");
}

/* beolvassa a l�p�sek list�j�t egy f�jlb�l */
void malom_lepesek_beolvas(Malom *m, FILE *fp) {
	Poz tempA;
	Poz tempB;
	char c1 = 0;
	char c2 = 0;
	while (1) {
		if (fscanf(fp, "%c%d;", &c1, &tempA.y) == 2) {
			tempA.x = (int)(c1-'a');
			malom_lepesek_beszur(m, l1, tempA, tempA);
		}
		else if (fscanf(fp, "x%c%d;", &c1, &tempA.y) == 2) {
			tempA.x = (int)(c1-'a');
			malom_lepesek_beszur(m, l2, tempA, tempA);
		}
		else if (fscanf(fp, "%c%d-%c%d;", &c1, &tempA.y, &c2, &tempB.y) == 4) {
			tempA.x = (int)(c1-'a');
			tempB.x = (int)(c2-'a');
			malom_lepesek_beszur(m, l3, tempA, tempB);
		}
		else
			return;
	}
}

/* megvizsg�lja, hogy l�tezik-e az adott f�jl */
bool malom_fajl_letezik(Malom *m) {
	if (fopen(m->fajlnev, "rt") != NULL)
		return true;
	return false;
}

/* lementi a j�t�k akt�lis �ll�s�t, egy f�jlba */
bool malom_fajl_mentes(Malom *m) {
	FILE *fp = fopen(m->fajlnev, "wt");
	if (fp == NULL)
		return false;
	for (int y = 0; y < 7; y++) {
		for (int x = 0; x < 7; x++)
			fputc(m->palya[y][x], fp);
		fputc('\n', fp);
	}
	fprintf(fp, "%d;%d;%d;%d;%d;%d;%d;%d;\n", m->palyaArany, m->jatekMod, m->jatekFazis, m->kovJ, m->kovL, m->poz.x, m->poz.y, m->debug);
	fprintf(fp, "%s;%d;%d;\n", m->j1.nev, m->j1.szin, m->j1.db);
	fprintf(fp, "%s;%d;%d;\n", m->j2.nev, m->j2.szin, m->j2.db);
	malom_lepesek_kiir(m,fp);
	fclose(fp);
	return true;
}

/* bet�lti a j�t�kot a megadott f�jlb�l */
bool malom_fajl_betoltes(Malom *m) {
	FILE *fp = fopen(m->fajlnev, "rt");
	if (fp == NULL)
		return false;
	for (int y = 0; y < 7; y++) {
		for (int x = 0; x < 7; x++)
			m->palya[y][x] = fgetc(fp);
		fgetc(fp);
	}
	fscanf(fp, "%d;%d;%d;%d;%d;%d;%d;%d;\n", &m->palyaArany, &m->jatekMod, &m->jatekFazis, &m->kovJ, &m->kovL, &m->poz.x, &m->poz.y, &m->debug);
	char tempNev1[101], tempNev2[101];
	fscanf(fp, "%[^;];%d;%d;\n", tempNev1, &m->j1.szin, &m->j1.db);
	m->j1.nev = (char*)malloc(strlen(tempNev1)+1);
	strcpy(m->j1.nev, tempNev1);
	fscanf(fp, "%[^;];%d;%d;\n", tempNev2, &m->j2.szin, &m->j2.db);
	m->j2.nev = (char*)malloc(strlen(tempNev2)+1);
	strcpy(m->j2.nev, tempNev2);
	malom_lepesek_beolvas(m,fp);
	fclose(fp);
	return true;
}
