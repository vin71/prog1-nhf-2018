#ifndef  MEGJELENITES_H_INCLUDED
#define  MEGJELENITES_H_INCLUDED

#include "vezerles.h"

/* beolvas a szabványos bemeneten keresztül, egy formailag és logikailag megfelelő pozíciót */
bool malom_poz_olvas(Malom *m);

/* a kirajzolást vezérlő függvény */
void malom_kirajzol(Malom *m);

#endif