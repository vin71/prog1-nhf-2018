#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "econio.h"
#include "vezerles.h"
#include "fajlkezeles.h"
#include "menu.h"

/* alapszinek meghatározása */
static EconioColor alap = LIGHTCYAN;
static EconioColor hiba = LIGHTRED;
static EconioColor fejlec = LIGHTYELLOW;

/** kiírja a menü fejlécét */
static void malom_menu_fejlec() {
	econio_textcolor(fejlec);
	printf("+--------------------+\n");
	printf("| M A L O M  M E N U |\n");
	printf("+--------------------+\n");
	econio_textcolor(alap);
}

/** beolvassa a felhasznaló által megadott számot, ami meg felel az intervalumnak [a;b] (ezzel a menüpontot választja ki) */
static int malom_menu_beolvas(int a, int b) {
	int n;
	scanf(" %d%*[^\n]", &n);
	while (n < a && n > a) {
		econio_textcolor(hiba);
		printf("\nNincs ilyen lehetoseg! Adj meg ujat: ");
		scanf(" %d%*[^\n]", &n);
		econio_textcolor(alap);
	}
	return n;
}

/** beolvas egy nevet a stdin-ról, és dinamikusan eltárolja egy temporális változó segítségével */
static void malom_nev_beolvas(Jatekos *j) {
	char tempNev[101];
	scanf(" %[^\n]s", tempNev);
	j->nev = (char*)malloc(strlen(tempNev) + 1);
	strcpy(j->nev, tempNev);
}

/** kiírja, hogy ki nyert! ILLETVE MEGHÍVJA A malom_free FÜGGVÉNYT! */
static void malom_menu_nyertes(Malom *m) {
	malom_menu_fejlec();
	econio_textcolor(m->kovJ == j1 ? m->j2.szin : m->j1.szin);
	printf("%s ", m->kovJ == j1 ? m->j2.nev : m->j1.nev);
	econio_textcolor(alap);
	printf("nyert!\n");
	malom_free(m);
	econio_sleep(6);
}

/** új játék előkészítése az adatok bekérésével */
static void malom_menu_jatek_uj() {
	/* mentés helyének beolvasása */
	malom_menu_fejlec();
	printf("Mentes helye(1-5): ");
	int mentes = malom_menu_beolvas(0,5);
	Malom *m = malom_uj();
	sprintf(m->fajlnev, "mentes%d.txt", mentes);
	econio_clrscr();
	/* pálya méretének beolvasása */
	malom_menu_fejlec();
	printf("Palya merete(1-10): ");
	m->palyaArany = malom_menu_beolvas(0,10);
	econio_clrscr();
	/* játékosok számának bekérése */
	malom_menu_fejlec();
	printf("Jatekosok szama(1-2): ");
	m->jatekMod = malom_menu_beolvas(1,2);
	econio_clrscr();
	/* játékosok nevének megadása */
	if (m->jatekMod == 1) {
		printf("A neved(MAX 100 karakter): ");
		malom_nev_beolvas(&m->j1);
		char tempNev[] = "Szamitogep";
		m->j2.nev = (char*)malloc(strlen(tempNev)+1);
		strcpy(m->j2.nev, tempNev);
		srand(time(NULL));
	}
	else {
		printf("Az elso jatekos neve(MAX 100 karakter): ");
		malom_nev_beolvas(&m->j1);
		econio_clrscr();
		printf("A masodik jatekos neve(MAX 100 karakter): ");
		malom_nev_beolvas(&m->j2);
	}
	malom_memoria_foglalva(m->j1.nev);
	malom_memoria_foglalva(m->j2.nev);
	/* játék elindítása */
	malom_jatek(m);
	malom_menu_nyertes(m);
}

/* betölti a játékot a megadott mentési fájlból */
static void malom_menu_jatek_betoltes() {
	malom_menu_fejlec();
	printf("Mentes(1-5): ");
	int mentes = malom_menu_beolvas(0,5);
	Malom *m = malom_uj();
	sprintf(m->fajlnev, "mentes%d.txt", mentes);
	if (!malom_fajl_betoltes(m)) {
		econio_textcolor(hiba);
		printf("NEM SIKERULT A BETOLTES!");
		econio_sleep(3);
		exit(0);
	}	
	/* játék elindítása */
	malom_jatek(m);
	malom_menu_nyertes(m);	
}

/* a játék menüje */
void malom_menu() {
	MenuPont menu;
	malom_menu_fejlec();
	printf("1. Uj jatek!\n");
	printf("2. Mentes betoltese!\n");
	printf("3. Kilepes!\n");
	printf("\nValassz menupontot! ");
	scanf(" %d%*[^\n]", &menu);
	econio_clrscr();
	switch (menu) {
		case 1:
			malom_menu_jatek_uj();
			break;
		case 2:
			malom_menu_jatek_betoltes();
			break;
		case 3:
			break;
		default:
			break;
	}	
}