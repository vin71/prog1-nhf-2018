#define _CRT_SECURE_NO_WARNINGS
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "econio.h"
#include "fajlkezeles.h"
#include "megjelenites.h"

/* a pálya tömbjének kiiratása (debug only) */
static void malom_ellenorzes(Malom *m) {
	for (int y = 0; y < 7; y++) {
		for (int x = 0; x < 7; x++)
			printf("%c ", m->palya[y][x]);
		printf("\n");
	}
	printf("palyaArany: %d jatekMod: %d jatekFazis: %d kovJ: %d kovL: %d poz.x: %d poz.y: %d\n", m->palyaArany, m->jatekMod, m->jatekFazis, m->kovJ, m->kovL, m->poz.x, m->poz.y);
	printf("j1: %s %d %d\n", m->j1.nev, m->j1.szin, m->j1.db);
	printf("j2: %s %d %d\n", m->j2.nev, m->j2.szin, m->j2.db);
}

/* beolvas a szabványos bemeneten keresztül, egy formailag és logikailag megfelelő pozíciót */
bool malom_poz_olvas(Malom *m) {
	if (m->jatekMod == 2 || m->kovJ == j1) {
		if (m->kovJ == j1) {
			econio_textcolor(m->j1.szin);
			printf("%s", m->j1.nev);
		}
		else {
			econio_textcolor(m->j2.szin);
			printf("%s", m->j2.nev);
		}
		econio_textcolor(LIGHTCYAN);
		if (m->kovL == l1)
			printf(" lep: - %s\n", "LETESZ");
		else if (m->kovL == l2)
			printf(" lep: - %s\n", "ELVESZ");
		else if (m->kovL == l3)
			printf(" lep: - %s - %s\n", "ATHELYEZ", "INNEN");
		else if (m->kovL == l4)
			printf(" lep: - %s - %s\n", "ATHELYEZ", "IDE");
		else if (m->kovL == l5)
			printf(" lep: - %s - %s\n", "UGRAS", "INNEN");
		else if (m->kovL == l6)
			printf(" lep: - %s - %s\n", "UGRAS", "IDE");
		char c1 = 0;
		char c2 = 0;
		if (scanf(" %c %c%*[^\n]", &c1, &c2) == 2) {
			if (c1 >= 'a' && c1 <= 'g' && c2 >= '0' && c2 <= '6') {
				m->poz.x = (int)(c1 - 'a');
				m->poz.y = (int)(c2 - '0');
				return true;
			}
			else if (c2 >= 'a' && c2 <= 'g' && c1 >= '0' && c1 <= '6') {
				m->poz.x = (int)(c2 - 'a');
				m->poz.y = (int)(c1 - '0');
				return true;
			}
		}
		return false;
	}
	else {
		m->poz.x = rand() % 7;
		m->poz.y = rand() % 7;
		return true;
	}
}

/* 
 * KIRAJZOLÁS SEGÉDFÜGGVÉNYEK
 */

/* igazat ad, ha babut kell kirajzolni */
static bool malom_kirajzol_babu(Malom *m, int x, int y) {
	if (m->palya[y][x] == '0' || m->palya[y][x] == '1' || m->palya[y][x] == '2')
		return true;
	return false;
}

static bool malom_kirajzol_vizszintes(Malom *m, int x, int y) {
	if (y == 3 && x == 2)
		return false;
	for (int i = x+1; i < 7; i++)
		if (malom_kirajzol_babu(m, y, i))
			return true;
	return false;
}

static bool malom_kirajzol_fuggoleges(Malom *m, int x, int y) {
	if (x == 3 && (y == 3 || y == 2))
		return false;
	bool alatta = false;
	bool felette = false;
	for (int i = y+1; i < 7; i++)
		if (malom_kirajzol_babu(m, i, x))
			alatta = true;
	for (int i = y; i >= 0; i--)
		if (malom_kirajzol_babu(m, i, x))
			felette = true;
	if (alatta && felette)
		return true;
	return false;
}

/* kirajzolja a pálya fejlécét, azaz a vízszintes koordinátákat */
static void malom_kirajzol_fejlec(int n) {
	for (int x = 0; x < 4; x++)
		printf(" ");
	for (int x = 0; x < 7; x++) {
		printf("%c%c", 'a'+x, 'a'+x);
		for (int i = 0; i < n * 2; i++)
			printf(" ");
	}
	printf("\n");
}

/* a pálya keretjének kirajzolását segíti (felső és alsó rész) */
static void malom_kirajzol_keret(int n) {
	printf("  +");
	for (int x = 0; x < (7+(7-1)*n)*2+2; x++)
		printf("-");
	printf("+\n");
}

/* a pálya keretjének bal, illetve jobb oldalát rajzolja ki */
static void malom_kirajzol_keret_oldal(int y, bool balOldal) {
	if (balOldal) {
		for (int x = 0; x < 2; x++)
			printf("%d", y);
		printf("| ");
	}
	else {
		printf(" |");
		for (int x = 0; x < 2; x++)
			printf("%d", y);
	}
}

/* */
static void malom_kirajzol_sorkoz(int y, int n, bool balOldal) {
	int szokoz = 0;
	if (y > 0 && y < 6 && y != 3)
		if (y % 2 == 0) szokoz = 2;
		else szokoz = 1;
	for (int x = 0; x < szokoz; x++) {
		if (balOldal) printf("||");
		for (int i = 0; i < n; i++) 
			printf("  ");
		if (!balOldal) printf("||");
	}
}

/* a kirajzolást vezérlő függvény */
void malom_kirajzol(Malom *m) {
	econio_textcolor(LIGHTCYAN);
	econio_clrscr();
	/* debug mód, ha m->debug egyenlő 1 */
	if(m->debug == 1)
		malom_ellenorzes(m);
	/* kiírja az eddigi lépéseket */
	printf("Lepesek: ");
	malom_lepesek_kiir(m, stdout);
	/* n változót inincializálom, mint m->palyaArany, hogy egyszerűbbek legyenek a képletek */
	int n = m->palyaArany;
	/* fejléc kirajzolása */
	malom_kirajzol_fejlec(n);
	/* keret felső részének kirajzolása */
	malom_kirajzol_keret(n);
	/* pálya kirajzolása */
	for (int y = 0; y < 7; y++) {
		/* baloldali keret kirajzolása */
		malom_kirajzol_keret_oldal(y, true);
		/* pálya kirajzolása */
		/* sorköz kirajzolás baloldalon */
		malom_kirajzol_sorkoz(y, n, true);
		for (int x = 0; x < 7; x++) {
			switch (m->palya[y][x]) {
			case '0':
				econio_textbackground(LIGHTCYAN);
				printf("  ");
				break;
			case '1':
				econio_textbackground(m->j1.szin);
				printf("  ");
				break;
			case '2':
				econio_textbackground(m->j2.szin);
				printf("  ");
				break;
			case '.':
				econio_textbackground(BLACK);
				for (int i = 0; i < n*2+1; i++)
					printf("  ");
				break;
			}
			econio_textbackground(BLACK);
			int szokoz = y!=3 ? abs(4-(y+1))-1 : 0;
			if (malom_kirajzol_babu(m, x, y) && malom_kirajzol_vizszintes(m, x, y)) {
				for (int i = 0; i < szokoz*(n+1)+n; i++)
					printf("==");
			}
		}
		/* sorköz kirajzolás jobboldalon */
		malom_kirajzol_sorkoz(y, n, false);
		/* jobboldali keret kirajzolása */
		malom_kirajzol_keret_oldal(y, false);
		/* közbelső sorok kirajzolása (a bábú nélküli sorok) */
		if (y != 7-1) {
			for (int i = 0; i < n; i++) {
				/* új sor */
				printf("\n");
				printf("  | ");
				for (int x = 0; x < 7; x++) {
					if (malom_kirajzol_fuggoleges(m, x, y))
						printf("||");
					else
						printf("  ");
					if (x != 7-1)
						for (int j = 0; j < n; j++)
							printf("  ");
				}
				printf(" |");
			}
		}
		/* új sor */
		if (y != 7-1) printf("\n");
	}
	/* új sor */
	printf("\n");
	/* keret alsó részének kirajzolása */
	malom_kirajzol_keret(n);
	/* lábléc kirajzolása */
	malom_kirajzol_fejlec(n);
}
