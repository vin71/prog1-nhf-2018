#ifndef  FAJLKEZELES_H_INCLUDED
#define  FAJLKEZELES_H_INCLUDED

#include "vezerles.h"

/* kiírja a lépések listáját a megadott kimenetre (fájl vagy stdout) */
void malom_lepesek_kiir(Malom *m, FILE *fp);

/* beolvassa a lépések listáját egy fájlból */
void malom_lepesek_beolvas(Malom *m, FILE *fp);

/* megvizsgálja, hogy létezik-e az adott fájl */
bool malom_fajl_letezik(Malom *m);

/* lementi a játék aktális állását, egy fájlba */
bool malom_fajl_mentes(Malom *m);

/* betölti a játékot a megadott fájlból */
bool malom_fajl_betoltes(Malom *m);

#endif