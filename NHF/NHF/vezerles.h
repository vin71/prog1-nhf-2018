#ifndef  VEZERLES_H_INCLUDED
#define  VEZERLES_H_INCLUDED

/* a játék aktulis fázisa */
typedef enum Fazis { f1, f2 } Fazis;

/* a következő játékos id-je */
typedef enum KovJ { j1, j2 } KovJ; 

/* a következő lépés típusa */
typedef enum LepesTipus { l1, l2, l3, l4, l5, l6 } LepesTipus;

/* x,y koordináta a pálya tömbjének kezeléséhez */
typedef struct Poz { 
	int x;
	int y;
} Poz;

/* egy játékos adatai */
typedef struct Jatekos { 
	char* nev;
	int szin;
	int db;
} Jatekos; 

/* duplán láncolt lista típus, amely egy lépést képes eltárolni */
typedef struct Lepes {
	LepesTipus lepes;
	Poz A;
	Poz B;
	struct Lepes *elozo, *kov;
} Lepes;

/* egy struktúra, amely a láncolt lista két strázsájának a címét tartalmazza */
typedef struct Lepesek {
	Lepes *elso;
	Lepes *utolso;
} Lepesek;

/* a játék fő struktúrája */
typedef struct Malom {
	char palya[7][7];
	int palyaArany;
	int jatekMod;
	Fazis jatekFazis;
	KovJ kovJ;
	LepesTipus kovL;
	Poz poz;
	Jatekos j1;
	Jatekos j2;
	Lepesek *lepesek;
	char fajlnev[12];
	int debug;
} Malom;

/* ellenőrzi, hogy sikerult e memóriat foglalni, az adott címen, ha nem akkor kilép */
void malom_memoria_foglalva(void *p);

/* az induló játék struktúrájának elkészítése */
Malom *malom_uj();

/* a lépések listájába szúrja be a sorra következő lépést */
void malom_lepesek_beszur(Malom* m, LepesTipus lepes, Poz A, Poz B);

/* a lefoglalt memóriaterület felszabadítása */
void malom_free(Malom *m);

/* a játék vezérlését végző függvény */
void malom_jatek(Malom *m); 

#endif